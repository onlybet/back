/**
 * Required External Modules
 */

import dotenv from 'dotenv-safe';
import { team, competition, game } from '@modules';
import { connectMongo } from '@/config/mongoose';
import { initServer } from '@/config/server';

dotenv.config({ example: '.declare.env' });
if (!process.env.PORT || !process.env.MONGO_API_USERNAME
  || !process.env.MONGO_API_PASSWORD || !process.env.MONGO_API_CLUSTER || !process.env.MONGO_API_DATABASE || !process.env.MODE
  || !process.env.CORS_ADDRESS) {
  process.exit(1);
}

// configure database
connectMongo(process.env.MONGO_API_USERNAME, process.env.MONGO_API_PASSWORD, process.env.MONGO_API_CLUSTER, process.env.MONGO_API_DATABASE);

// run serve
const server = initServer();

// link controllers
server.use('/teams', team.controller.instance);
server.use('/competitions/foot', competition.foot.controller.instance);
server.use('/games/foot', game.foot.controller.instance);

// start server
const PORT: number = parseInt(process.env.PORT as string, 10);
server.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
