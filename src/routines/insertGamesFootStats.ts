/* eslint-disable import/order */
import dotenv from 'dotenv-safe';
import { connectMongo } from '../config/mongoose';
import axios from 'axios';
import { GameFootModel } from '../modules/Game/foot/gameFoot.model';
import { GameFootStatsModel } from '../modules/Game/foot/stats/gameFootStats.model';

const API_URL = 'https://api-football-v1.p.rapidapi.com/v3';

dotenv.config({ example: '.declare.env' });
if (!process.env.PORT
|| !process.env.MONGO_USERNAME || !process.env.MONGO_PASSWORD || !process.env.MONGO_CLUSTER || !process.env.MONGO_DATABASE
|| !process.env.MONGO_API_USERNAME || !process.env.MONGO_API_PASSWORD || !process.env.MONGO_API_CLUSTER || !process.env.MONGO_API_DATABASE
|| !process.env.MODE) {
  process.exit(1);
}

// configure database
connectMongo(process.env.MONGO_API_USERNAME, process.env.MONGO_API_PASSWORD, process.env.MONGO_API_CLUSTER, process.env.MONGO_API_DATABASE);

interface Statistics {
  [index: number] : {
    value: number | string | null;
  }
}

interface APIGameFootStats {
  statistics: Statistics;
}

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

GameFootModel.find({
  $and: [
    { $or: [{ shortStatus: 'FT' }, { shortStatus: 'AET' }, { shortStatus: 'PEN' }] },
    { date: { $lte: new Date() } }],
}, async (err, games) => {
  let i: number = 1;
  // eslint-disable-next-line no-restricted-syntax
  for await (const game of games) {
    if (i % 290 === 0) {
      console.log('Sleep pour 1min');
      await sleep(120000);
    }
    console.log(game);
    const optionsGamesFootStats = {
      params: { fixture: game._id },
      headers: {
        'x-rapidapi-host': 'api-football-v1.p.rapidapi.com',
        'x-rapidapi-key': '14f0b38968mshd9e497e9ea30f90p1ce1cejsnb33dcc2ff713',
      },
    };

    axios.get(`${API_URL}/fixtures/statistics`, { ...optionsGamesFootStats }).then((resp) => {
      const infosGamesFootStats = resp.data.response;
      console.log(infosGamesFootStats);
      console.log(`id game : ${game._id}`);

      const insertGamesFootStats = new GameFootStatsModel({
        _id: game._id,
        teamHome: {
          shotsOnGoal: infosGamesFootStats[0].statistics[0].value,
          shotsOffGoal: infosGamesFootStats[0].statistics[1].value,
          totalShots: infosGamesFootStats[0].statistics[2].value,
          blockedShots: infosGamesFootStats[0].statistics[3].value,
          shotsInsidebox: infosGamesFootStats[0].statistics[4].value,
          shotsOutsidebox: infosGamesFootStats[0].statistics[5].value,
          fouls: infosGamesFootStats[0].statistics[6].value,
          cornerKicks: infosGamesFootStats[0].statistics[7].value,
          offsides: infosGamesFootStats[0].statistics[8].value,
          ballPossession: infosGamesFootStats[0].statistics[9].value,
          yellowCards: infosGamesFootStats[0].statistics[10].value,
          redCards: infosGamesFootStats[0].statistics[11].value,
          goalkeeperSaves: infosGamesFootStats[0].statistics[12].value,
          totalPasses: infosGamesFootStats[0].statistics[13].value,
          passesAccurate: infosGamesFootStats[0].statistics[14].value,
          passesPourcent: infosGamesFootStats[0].statistics[15].value,
        },
        teamAway: {
          shotsOnGoal: infosGamesFootStats[1].statistics[0].value,
          shotsOffGoal: infosGamesFootStats[1].statistics[1].value,
          totalShots: infosGamesFootStats[1].statistics[2].value,
          blockedShots: infosGamesFootStats[1].statistics[3].value,
          shotsInsidebox: infosGamesFootStats[1].statistics[4].value,
          shotsOutsidebox: infosGamesFootStats[1].statistics[5].value,
          fouls: infosGamesFootStats[1].statistics[6].value,
          cornerKicks: infosGamesFootStats[1].statistics[7].value,
          offsides: infosGamesFootStats[1].statistics[8].value,
          ballPossession: infosGamesFootStats[1].statistics[9].value,
          yellowCards: infosGamesFootStats[1].statistics[10].value,
          redCards: infosGamesFootStats[1].statistics[11].value,
          goalkeeperSaves: infosGamesFootStats[1].statistics[12].value,
          totalPasses: infosGamesFootStats[1].statistics[13].value,
          passesAccurate: infosGamesFootStats[1].statistics[14].value,
          passesPourcent: infosGamesFootStats[1].statistics[15].value,
        },
      });
      insertGamesFootStats.save((error : any) => {
        if (error) console.log(`error save stats${error}`);
        else console.log('Success');
      });
    });
    i += 1;
  }
});
