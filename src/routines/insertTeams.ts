/* eslint-disable import/order */
import dotenv from 'dotenv-safe';
import { connectMongo } from '../config/mongoose';
import axios from 'axios';
import { TeamModel } from '../modules/team/team.model';

const API_URL = 'https://api-football-v1.p.rapidapi.com/v3';

dotenv.config({ example: '.declare.env' });
if (!process.env.PORT
|| !process.env.MONGO_USERNAME || !process.env.MONGO_PASSWORD || !process.env.MONGO_CLUSTER || !process.env.MONGO_DATABASE
|| !process.env.MONGO_API_USERNAME || !process.env.MONGO_API_PASSWORD || !process.env.MONGO_API_CLUSTER || !process.env.MONGO_API_DATABASE
|| !process.env.MODE) {
  process.exit(1);
}

// configure database
connectMongo(process.env.MONGO_API_USERNAME, process.env.MONGO_API_PASSWORD, process.env.MONGO_API_CLUSTER, process.env.MONGO_API_DATABASE);

interface topLeagues {
  country: string;
  league: string;
}
const _leagues: Array<topLeagues> = [
  { country: 'Spain', league: 'La Liga' },
  { country: 'France', league: 'Ligue 1' },
  { country: 'Italy', league: 'Serie A' },
  { country: 'Germany', league: 'Bundesliga 1' },
  { country: 'England', league: 'Premier League' },
];

type APICompetitionType = 'league' | 'cup';

interface APICompetition {
  league: {
    id: number;
    name: string;
    type: APICompetitionType;
    logo: string;
  };
  seasons: [{
    year: number;
    start: Date;
    end: Date;
    current: boolean;
  }];
}

interface APITeam {
  team: {
    id: number;
    name: string;
    country: string;
    logo: string;
  };
}

function getCompetitions(param: topLeagues) {
  const optionsCompetitions = {
    params: { name: param.league, country: param.country, current: 'true' },
    headers: {
      'x-rapidapi-host': 'api-football-v1.p.rapidapi.com',
      'x-rapidapi-key': '14f0b38968mshd9e497e9ea30f90p1ce1cejsnb33dcc2ff713',
    },
  };
  return axios.get(`${API_URL}/leagues`, { ...optionsCompetitions });
}

_leagues.forEach((comp) => {
  console.log(`league : ${comp.league}`);
  getCompetitions(comp).then((res) => {
    const infosCompetitions: Array<APICompetition> = res.data.response;
    infosCompetitions.forEach((competition : APICompetition) => {
      const optionsTeams = {
        params: { league: competition.league.id, season: competition.seasons[0].year },
        headers: {
          'x-rapidapi-host': 'api-football-v1.p.rapidapi.com',
          'x-rapidapi-key': '14f0b38968mshd9e497e9ea30f90p1ce1cejsnb33dcc2ff713',
        },
      };

      axios.get(`${API_URL}/teams`, { ...optionsTeams }).then((resp) => {
        const infosTeams: Array<APITeam> = resp.data.response;

        infosTeams.forEach((element) => {
          const insertTeams = new TeamModel({
            _id: element.team.id, name: element.team.name, country: element.team.country, logo: element.team.logo,
          });
          insertTeams.save((err) => {
            if (err) console.log(`error save${err}`);
            else console.log('Success');
          });
        });
      });
    });
  }).catch((error: any) => {
    console.error(error);
  });
});
