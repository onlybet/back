/* eslint-disable import/order */
import dotenv from 'dotenv-safe';
import { connectMongo } from '../config/mongoose';
import { CompetitionFootModel } from '../modules/competition/foot/competitionFoot.model';

const axios = require('axios').default;

dotenv.config({ example: '.declare.env' });
if (!process.env.PORT
|| !process.env.MONGO_USERNAME || !process.env.MONGO_PASSWORD || !process.env.MONGO_CLUSTER || !process.env.MONGO_DATABASE
|| !process.env.MONGO_API_USERNAME || !process.env.MONGO_API_PASSWORD || !process.env.MONGO_API_CLUSTER || !process.env.MONGO_API_DATABASE
|| !process.env.MODE) {
  process.exit(1);
}

// configure database
connectMongo(process.env.MONGO_API_USERNAME, process.env.MONGO_API_PASSWORD, process.env.MONGO_API_CLUSTER, process.env.MONGO_API_DATABASE);

const leagues = new Map([
  ['Spain', 'La Liga'],
  ['France', 'Ligue 1'],
  ['Italy', 'Serie A'],
  ['Germany', 'Bundesliga 1'],
  ['England', 'Premier League'],
]);

const leaguesIter = leagues.keys();

leagues.forEach((element) => {
  const options = {
    method: 'GET',
    url: 'https://api-football-v1.p.rapidapi.com/v3/leagues',
    params: { name: element, country: leaguesIter.next().value, current: 'true' },
    headers: {
      'x-rapidapi-host': 'api-football-v1.p.rapidapi.com',
      'x-rapidapi-key': '14f0b38968mshd9e497e9ea30f90p1ce1cejsnb33dcc2ff713',
    },
  };

  axios.request(options).then((response: { data: any; }) => {
    const competitionsFoot = response.data.response[0];
    const insertCompetitionFoot = new CompetitionFootModel({
      _id: competitionsFoot.league.id,
      name: competitionsFoot.league.name,
      country: competitionsFoot.country.name,
      season: competitionsFoot.seasons[0].year,
      logo: competitionsFoot.league.logo,
      type: competitionsFoot.league.type,
      active: competitionsFoot.seasons[0].current,
    });
    insertCompetitionFoot.save((err) => {
      if (err) console.log(`error save${err}`);
      else console.log('Success');
    });
  }).catch((error: any) => {
    console.error(error);
  });
});
