/* eslint-disable import/order */
import dotenv from 'dotenv-safe';
import { connectMongo } from '../config/mongoose';
import axios from 'axios';
import { TeamModel } from '../modules/team/team.model';
import { PlayerFootModel } from '../modules/PlayerFoot/playerFoot.model';
import { CompetitionFootModel } from '../modules/competition/foot/competitionFoot.model';

const API_URL = 'https://api-football-v1.p.rapidapi.com/v3';

dotenv.config({ example: '.declare.env' });
if (!process.env.PORT
|| !process.env.MONGO_USERNAME || !process.env.MONGO_PASSWORD || !process.env.MONGO_CLUSTER || !process.env.MONGO_DATABASE
|| !process.env.MONGO_API_USERNAME || !process.env.MONGO_API_PASSWORD || !process.env.MONGO_API_CLUSTER || !process.env.MONGO_API_DATABASE
|| !process.env.MODE) {
  process.exit(1);
}

// configure database
connectMongo(process.env.MONGO_API_USERNAME, process.env.MONGO_API_PASSWORD, process.env.MONGO_API_CLUSTER, process.env.MONGO_API_DATABASE);

interface APIPlayerFoot {
  player: {
    id: number;
    name: string;
    firstname: string;
    lastname: string;
    nationality: string;
    photo: string;
  };
  statistics: [{
    games: {
      position: string;
    }
  }];
}

CompetitionFootModel.find({}, (errCompetitions, competitions) => {
  competitions.forEach((competition) => {
    TeamModel.find({ country: competition.country }, (errTeams, teams) => {
      // let i = 0;
      teams.forEach(async (team) => {
        const optionsPlayersFoot = {
          params: { team: team.id, league: competition.id, season: competition.season },
          headers: {
            'x-rapidapi-host': 'api-football-v1.p.rapidapi.com',
            'x-rapidapi-key': '14f0b38968mshd9e497e9ea30f90p1ce1cejsnb33dcc2ff713',
          },
        };

        axios.get(`${API_URL}/players`, { ...optionsPlayersFoot }).then((resp) => {
          const infosPlayersFoot: Array<APIPlayerFoot> = resp.data.response;

          infosPlayersFoot.forEach((element) => {
            const insertPlayersFoot = new PlayerFootModel({
              id: element.player.id,
              name: element.player.name,
              firstname: element.player.firstname,
              lastname: element.player.lastname,
              nationality: element.player.nationality,
              photo: element.player.photo,
              idEquipe: team.id,
              position: element.statistics[0].games.position,
              numero: team.id,
            });
            insertPlayersFoot.save((err) => {
              if (err) console.log(`error save${err}`);
              else console.log('Success');
            });
          });
        });

        // i += 1;
        // if (i === 90) {
        //   console.log('sleep');
        //   await new Promise((f) => setTimeout(f, 90000));
        // }
      });
    });
  });
});
