/* eslint-disable import/order */
import dotenv from 'dotenv-safe';
import { connectMongo } from '../config/mongoose';
import axios from 'axios';
import { CompetitionFootModel } from '../modules/competition/foot/competitionFoot.model';
import { GameFootModel } from '../modules/Game/foot/gameFoot.model';

const API_URL = 'https://api-football-v1.p.rapidapi.com/v3';

dotenv.config({ example: '.declare.env' });
if (!process.env.PORT
|| !process.env.MONGO_USERNAME || !process.env.MONGO_PASSWORD || !process.env.MONGO_CLUSTER || !process.env.MONGO_DATABASE
|| !process.env.MONGO_API_USERNAME || !process.env.MONGO_API_PASSWORD || !process.env.MONGO_API_CLUSTER || !process.env.MONGO_API_DATABASE
|| !process.env.MODE) {
  process.exit(1);
}

// configure database
connectMongo(process.env.MONGO_API_USERNAME, process.env.MONGO_API_PASSWORD, process.env.MONGO_API_CLUSTER, process.env.MONGO_API_DATABASE);

interface APIGameFoot {
  fixture: {
    id: number;
    date: Date;
    status:{
      long: string;
      short: string;
      elapsed: number;
    }
  };
  league: {
    id: number;
    round: string;
  };
  teams: {
    home: {
      id: number;
      name: string;
    };
    away: {
      id: number;
      name: string;
    };
  };
  goals: {
    home: number;
    away: number;
  };
}

CompetitionFootModel.find({}, (err, competitions) => {
  competitions.forEach((competition) => {
    console.log(competition);
    const optionsGamesFoot = {
      params: { league: competition._id, season: competition.season },
      headers: {
        'x-rapidapi-host': 'api-football-v1.p.rapidapi.com',
        'x-rapidapi-key': '14f0b38968mshd9e497e9ea30f90p1ce1cejsnb33dcc2ff713',
      },
    };

    axios.get(`${API_URL}/fixtures`, { ...optionsGamesFoot }).then((resp) => {
      const infosGamesFoot: Array<APIGameFoot> = resp.data.response;

      const i = 0;
      infosGamesFoot.forEach((element) => {
        const insertGamesFoot = new GameFootModel({
          _id: element.fixture.id,
          date: element.fixture.date,
          idCompetition: competition._id,
          teams: {
            idTeamHome: element.teams.home.id,
            idTeamAway: element.teams.away.id,
          },
          goals: {
            goalsTeamHome: element.goals.home,
            goalsTeamAway: element.goals.away,
          },
          round: element.league.round,
          longStatus: element.fixture.status.long,
          shortStatus: element.fixture.status.short,
          elapsed: element.fixture.status.elapsed,
        });
        insertGamesFoot.save((error : any) => {
          if (error) console.log(`error save${error}`);
          else console.log(`Success : ${i}`);
        });
      });
    });
  });
});
