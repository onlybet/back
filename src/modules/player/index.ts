import controller from './player.controller';
import service from './player.service';
import { PlayerSchema, PlayerModel, Player } from './player.model';

const player = {
  controller,
  service,
  schema: PlayerSchema,
  model: PlayerModel,
};
export default player;

export type { Player };
