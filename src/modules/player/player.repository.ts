import { PlayerModel, Player } from './player.model';

const getAll = async (): Promise<Player[]> => {
  const players = await PlayerModel.find().lean();
  return players.map((player) => ({ ...player, id: player.id }));
};

const getById = async (id: string): Promise<Player | null> => {
  try {
    const player = await PlayerModel.findById(id);
    if (player === null) throw Error(`No player found with ID ${id}.`);
    return { ...player.toJSON(), id: player.id };
  } catch (e: unknown) {
    throw Error((e as {message: string}).message);
  }
};

export default {
  getAll, getById,
};
