import express from 'express';
import playerService from './player.service';

const controller = express.Router();

controller.get('/players', playerService.getAll);

export default { instance: controller };
