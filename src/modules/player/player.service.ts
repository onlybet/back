import playerRepository from './player.repository';
import { Player } from './player.model';

const getAll = async (): Promise<Player[]> => playerRepository.getAll();

const getPlayerById = async (id: string): Promise<Player> => {
  try {
    const player = await playerRepository.getById(id);
    if (player === null) throw Error(`No player found with id ${id}`);
    return player;
  } catch (e:any) {
    throw Error(e.message);
  }
};

export default { getPlayerById, getAll };
