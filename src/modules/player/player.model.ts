import { model, Schema } from 'mongoose';

export interface Player{
  id: number;
  name: string;
  firstname: string;
  lastname: string;
  nationality: string;
  photo: string;
}

export const PlayerSchema = new Schema<Player>({
  id: {
    type: Number,
  },
  name: {
    type: String, required: true, trim: true, set: (n: string) => n.trim().substr(0, 50),
  },
  firstname: {
    type: String, required: true, trim: true, set: (n: string) => n.trim().substr(0, 50),
  },
  lastname: {
    type: String, required: true, trim: true, set: (n: string) => n.trim().substr(0, 50),
  },
  nationality: {
    type: String, required: true, trim: true, set: (n: string) => n.trim().substr(0, 50),
  },
  photo: {
    type: String, required: true, trim: true, set: (n: string) => n.trim().substr(0, 50),
  },
}, { timestamps: true });

export const PlayerModel = model<Player>('Player', PlayerSchema);
