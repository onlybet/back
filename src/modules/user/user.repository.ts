// import { UserModel, User } from './user.model';

// const save = async (data: {email: string, birthdate: Date, fullName?: string}): Promise<User> => {
//   const user = (await UserModel.findOneAndUpdate({ ...data }, {}, { new: true, upsert: true, runValidators: true })).toJSON();
//   return { ...user, _id: user._id };
// };

// const set = async (id: number, data: Partial<User>): Promise<User | null> => {
//   try {
//     const result = await UserModel.findByIdAndUpdate(id, data, { runValidators: true, new: true });
//     if (result === null) return null;
//     const user = result.toJSON();
//     return { ...user, _id: user._id };
//   } catch {
//     throw Error(`Can't update user ${id}, wrong data format.`);
//   }
// };

// const getById = async (id: number): Promise<User | null> => {
//   try {
//     const user = await UserModel.findById(id);
//     if (user === null) throw Error(`No user found with ID ${id}.`);
//     return { ...user.toJSON(), _id: user._id };
//   } catch (e: unknown) {
//     throw Error((e as {message: string}).message);
//   }
// };

// export default { set, save, getById };
