// import { model, Schema } from 'mongoose';

// export interface User{
//   _id: number;
//   email: string;
//   birthdate?: Date;
//   firstName?: string;
//   lastName?: string;
//   fullName?: string;
//   address?: string;
//   complementaryAddress?: string;
//   country?: string;
//   city?: string;
//   ZIPCode?: string;
//   favorites: {
//     idsCompetitions: number[];
//     idsTeams: number[];
//   }
// }

// export const UserSchema = new Schema<User>({
//   _id: {
//     type: Number, required: true,
//   },
//   email: {
//     type: String, required: true, trim: true, set: (n: string) => n.trim().substr(0, 50),
//   },
//   birthdate: {
//     type: Date, required: true,
//   },
//   fullName: {
//     type: String, trim: true, minLength: 2, set: (n: string) => n.trim().substr(0, 80),
//   },
//   lastName: {
//     type: String, trim: true, minLength: 2, set: (n: string) => n.trim().substr(0, 80),
//   },
//   address: {
//     type: String, trim: true, minLength: 3, set: (n: string) => n.trim().substr(0, 200),
//   },
//   complementaryAddress: {
//     type: String, trim: true, minLength: 3, set: (n: string) => n.trim().substr(0, 200),
//   },
//   country: {
//     type: String, trim: true, minLength: 3, set: (n: string) => n.trim().substr(0, 80),
//   },
//   city: {
//     type: String, trim: true, minLength: 3, set: (n: string) => n.trim().substr(0, 80),
//   },
//   ZIPCode: {
//     type: String, trim: true, minLength: 2, set: (n: string) => n.trim().substr(0, 20),
//   },
//   favorites: {
//     idsCompetitions: {
//       type: Array, items: { type: Number },
//     },
//     idsTeams: {
//       type: Array, items: { type: Number },
//     },
//   },
// }, { timestamps: true });

// export const UserModel = model<User>('User', UserSchema);
