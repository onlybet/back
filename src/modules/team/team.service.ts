import teamRepository from './team.repository';
import { Team } from './team.model';

const getAll = async (): Promise<Team[]> => {
  try {
    const teams = await teamRepository.getAll();
    if (teams === null) throw Error('No teams found');
    return teams;
  } catch (e:any) {
    throw Error(e.message);
  }
};

const getTeamById = async (id: number): Promise<Team> => {
  try {
    const team = await teamRepository.getById(id);
    if (team === null) throw Error(`No team found with id ${id}`);
    return team;
  } catch (e:any) {
    throw Error(e.message);
  }
};

const getTeamsByName = async (name: string): Promise<Team[]> => {
  try {
    const teams = await teamRepository.getByName(name);
    if (teams === null) throw Error(`No teams found with name ${name}`);
    return teams;
  } catch (e:any) {
    throw Error(e.message);
  }
};

export default { getTeamById, getAll, getTeamsByName };
