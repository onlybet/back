import { TeamModel, Team } from './team.model';

const getAll = async (): Promise<Team[]> => {
  try {
    const teams = await TeamModel.find().lean();
    return teams.map((team) => ({ ...team, id: team._id }));
  } catch (e: unknown) {
    throw Error((e as {message: string}).message);
  }
};

const getById = async (id: number): Promise<Team | null> => {
  try {
    const team = await TeamModel.findById(id);
    if (team === null) throw Error(`No team found with ID ${id}.`);
    return { ...team.toJSON() };
  } catch (e: unknown) {
    throw Error((e as {message: string}).message);
  }
};

const getByName = async (name: string): Promise<Team[]> => {
  try {
    // eslint-disable-next-line quote-props
    const teams = await TeamModel.find({ name: { '$regex': name, '$options': 'i' } }).lean();
    return teams.map((team) => ({ ...team, id: team._id }));
  } catch (e: unknown) {
    throw Error((e as {message: string}).message);
  }
};

export default {
  getAll, getById, getByName,
};
