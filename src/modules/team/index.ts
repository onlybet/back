import controller from './team.controller';
import service from './team.service';
import { TeamSchema, TeamModel, Team } from './team.model';

const team = {
  controller,
  service,
  schema: TeamSchema,
  model: TeamModel,
};
export default team;

export type { Team };
