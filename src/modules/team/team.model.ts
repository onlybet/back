import { model, Schema } from 'mongoose';

export interface Team{
  _id: number;
  name: string;
  country: string;
  logo: string;
}

export const TeamSchema = new Schema<Team>({
  _id: {
    type: Number, required: true,
  },
  name: {
    type: String, required: true, trim: true, set: (n: string) => n.trim().substr(0, 50),
  },
  country: {
    type: String, required: true, trim: true, set: (n: string) => n.trim().substr(0, 50),
  },
  logo: {
    type: String, required: true,
  },
}, { timestamps: true });

export const TeamModel = model<Team>('Team', TeamSchema);
