/* eslint-disable linebreak-style */
import { model, Schema } from 'mongoose';
import { Player } from '../player/player.model';

export interface PlayerFoot extends Player{
  idEquipe: number;
  position: string;
  numero: number;
}

export const PlayerFootSchema = new Schema<PlayerFoot>({
  id: {
    type: Number,
  },
  name: {
    type: String, required: true, trim: true, set: (n: string) => n.trim().substr(0, 50),
  },
  firstname: {
    type: String, required: true, trim: true, set: (n: string) => n.trim().substr(0, 50),
  },
  lastname: {
    type: String, required: true, trim: true, set: (n: string) => n.trim().substr(0, 50),
  },
  nationality: {
    type: String, required: true, trim: true, set: (n: string) => n.trim().substr(0, 50),
  },
  photo: {
    type: String, required: true, trim: true, set: (n: string) => n.trim().substr(0, 50),
  },
  idEquipe: {
    type: Number, required: true,
  },
  position: {
    type: String, required: true, trim: true, set: (n: string) => n.trim().substr(0, 50),
  },
  numero: {
    type: Number, required: true,
  },
}, { timestamps: true });

export const PlayerFootModel = model<PlayerFoot>('PlayerFoot', PlayerFootSchema);
