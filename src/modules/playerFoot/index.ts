import controller from './playerFoot.controller';
import service from './playerFoot.service';
import { PlayerFootSchema, PlayerFootModel, PlayerFoot } from './playerFoot.model';

const playerFoot = {
  controller,
  service,
  schema: PlayerFootSchema,
  model: PlayerFootModel,
};
export default playerFoot;

export type { PlayerFoot };
