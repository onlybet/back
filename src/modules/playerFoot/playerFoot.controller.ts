import express from 'express';
import playerFootService from './playerFoot.service';

const controller = express.Router();

controller.get('/playersFoot', playerFootService.getAll);

export default { instance: controller };
