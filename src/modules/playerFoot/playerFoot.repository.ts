import { PlayerFootModel, PlayerFoot } from './playerFoot.model';

const getAll = async (): Promise<PlayerFoot[]> => {
  const playerFoots = await PlayerFootModel.find().lean();
  return playerFoots.map((playerFoot) => ({ ...playerFoot, id: playerFoot.id }));
};

const getById = async (id: string): Promise<PlayerFoot | null> => {
  try {
    const playerFoot = await PlayerFootModel.findById(id);
    if (playerFoot === null) throw Error(`No playerFoot found with ID ${id}.`);
    return { ...playerFoot.toJSON(), id: playerFoot.id };
  } catch (e: unknown) {
    throw Error((e as {message: string}).message);
  }
};

export default {
  getAll, getById,
};
