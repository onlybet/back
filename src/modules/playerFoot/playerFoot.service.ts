import playerFootRepository from './playerFoot.repository';
import { PlayerFoot } from './playerFoot.model';

const getAll = async (): Promise<PlayerFoot[]> => playerFootRepository.getAll();

const getPlayerFootById = async (id: string): Promise<PlayerFoot> => {
  try {
    const playerFoot = await playerFootRepository.getById(id);
    if (playerFoot === null) throw Error(`No playerFoot found with id ${id}`);
    return playerFoot;
  } catch (e:any) {
    throw Error(e.message);
  }
};

export default { getPlayerFootById, getAll };
