import express, { Request, Response } from 'express';
import gameFootService from './gameFoot.service';
import competitionFootService from '../../competition/foot/competitionFoot.service';
import teamService from '../../team/team.service';

const controller = express.Router();

controller.get('/all', async (req: Request, res: Response) => {
  try {
    const result = await gameFootService.getAll();
    res.status(200).send(result);
  } catch (e) {
    res.status(500).send((e as any).message);
  }
});

controller.get('/:id', async (req: Request, res: Response) => {
  try {
    const game = await gameFootService.getGameFootById(Number(req.params.id));

    const teamHome = teamService.getTeamById(game.teams.idTeamHome);
    const teamAway = teamService.getTeamById(game.teams.idTeamAway);
    const competition = competitionFootService.getCompetitionFootById(game.idCompetition);
    const data = await Promise.all([teamHome, teamAway, competition]);

    const teams = { ...game.teams, teamHome: { ...data[0], id: data[0]._id }, teamAway: { ...data[1], id: data[1]._id } };
    const finalGameFormat = {
      ...game, competition: data[2], teams, /* active: isActive, */
    };

    res.status(200).send(finalGameFormat);
  } catch (e) {
    res.status(500).send((e as any).message);
  }
});

controller.get('/competition/:idComp', async (req: Request, res: Response) => {
  try {
    let compGameByRound;
    let allCompGames: Array<any>;
    const nextCompGame = await gameFootService.getNextGameFootByIdCompetition(Number(req.params.idComp));

    if (req.query.round !== null && req.query.round !== undefined) {
      compGameByRound = await gameFootService.getGameFootByRoundAndIdComp(Number(req.params.idComp), req.query.round.toString());
      allCompGames = [];
    } else {
      compGameByRound = await gameFootService.getGameFootByRoundAndIdComp(Number(req.params.idComp), nextCompGame.round);
      allCompGames = (await gameFootService.getGameFootByIdCompetition(Number(req.params.idComp))).slice(0, 200);
    }

    const addTeams = async (list: Array<any>) => {
      const promises = [];
      for (let i = 0; i < list.length; i += 1) {
        const game = list[i];
        const teamHome = teamService.getTeamById(game.teams.idTeamHome);
        const teamAway = teamService.getTeamById(game.teams.idTeamAway);

        // eslint-disable-next-line no-await-in-loop
        promises.push(teamHome);
        promises.push(teamAway);
      }

      const result = await Promise.all(promises);

      const allGames = [];
      for (let i = 0; i < result.length; i += 2) {
        const teamHome = result[i];
        const teamAway = result[i + 1];

        if (list[i]) {
          allGames.push({
            ...list[i],
            teams: { ...list[i].teams, teamHome, teamAway },
          });
        }
      }
      return allGames;
    };

    const allGames = await addTeams(allCompGames);
    const gamesByRound = await addTeams(compGameByRound);

    const getRounds = (game: any) => {
      const previousRound: string = `${game.round.split('- ')[0]}- ${Number(game.round.split('- ')[1]) - 1}`;
      const nextRound: string = `${game.round.split('- ')[0]}- ${Number(game.round.split('- ')[1]) + 1}`;
      return { previous: previousRound, current: game.round, next: nextRound };
    };

    const rounds = getRounds(compGameByRound[0] ? compGameByRound[0] : nextCompGame);

    const response = {
      games: allGames, nextGame: nextCompGame, gamesByRound, rounds, /* active: isActive, */
    };

    res.status(200).send(response);
  } catch (e) {
    res.status(500).send((e as any).message);
  }
});

controller.get('/competition/:idComp/next', async (req: Request, res: Response) => {
  try {
    const result = await gameFootService.getNextGameFootByIdCompetition(Number(req.params.idComp));
    res.status(200).send(result);
  } catch (e) {
    res.status(500).send((e as any).message);
  }
});

controller.get('/competition/:idComp/previous', async (req: Request, res: Response) => {
  try {
    const result = await gameFootService.getPreviousGameFootByIdCompetition(Number(req.params.idComp));
    res.status(200).send(result);
  } catch (e) {
    res.status(500).send((e as any).message);
  }
});

controller.get('/team/:idTeam', async (req: Request, res: Response) => {
  try {
    const result = await gameFootService.getGameFootByIdTeam(Number(req.params.idTeam));
    res.status(200).send(result);
  } catch (e) {
    res.status(500).send((e as any).message);
  }
});

controller.get('/team/:idTeam/next', async (req: Request, res: Response) => {
  try {
    const result = await gameFootService.getNextGameFootByIdTeam(Number(req.params.idTeam));
    res.status(200).send(result);
  } catch (e) {
    res.status(500).send((e as any).message);
  }
});

controller.get('/team/:idTeam/previous', async (req: Request, res: Response) => {
  try {
    const games = await gameFootService.getPreviousGameFootByIdTeam(Number(req.params.idTeam));
    const result: Array<any> = [];

    // eslint-disable-next-line no-restricted-syntax
    for await (const game of games) {
      const teamHome = teamService.getTeamById(game.teams.idTeamHome);
      const teamAway = teamService.getTeamById(game.teams.idTeamAway);
      const competition = competitionFootService.getCompetitionFootById(game.idCompetition);
      const data = await Promise.all([teamHome, teamAway, competition]);

      // const isActive = game.date < new Date(game.date);

      const teams = { ...game.teams, teamHome: { ...data[0], id: data[0]._id }, teamAway: { ...data[1], id: data[1]._id } };
      const finalGameFormat = {
        ...game, competition: data[2], teams, /* active: isActive, */
      };
      result.push(finalGameFormat);
    }

    res.status(200).send(result);
  } catch (e) {
    res.status(500).send((e as any).message);
  }
});

controller.get('/date/:timestamp', async (req: Request, res: Response) => {
  try {
    const games = await gameFootService.getGameFootByDate(new Date(Number(req.params.timestamp)));
    const result: Array<any> = [];

    // eslint-disable-next-line no-restricted-syntax
    for await (const game of games) {
      const teamHome = teamService.getTeamById(game.teams.idTeamHome);
      const teamAway = teamService.getTeamById(game.teams.idTeamAway);
      const competition = competitionFootService.getCompetitionFootById(game.idCompetition);
      const data = await Promise.all([teamHome, teamAway, competition]);

      // const isActive = game.date < new Date(game.date);

      const teams = { ...game.teams, teamHome: { ...data[0], id: data[0]._id }, teamAway: { ...data[1], id: data[1]._id } };
      const finalGameFormat = {
        ...game, competition: data[2], teams, /* active: isActive, */
      };
      result.push(finalGameFormat);
    }

    res.status(200).send(result);
  } catch (e) {
    res.status(500).send((e as any).message);
  }
});

export default { instance: controller };
