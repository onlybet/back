import { GameFootModel, GameFoot } from './gameFoot.model';

const getAll = async (): Promise<GameFoot[]> => {
  try {
    const games = await GameFootModel.find().lean();
    return games.map((game) => ({ ...game, id: game._id }));
  } catch (e: unknown) {
    throw Error((e as {message: string}).message);
  }
};

const getById = async (id: number): Promise<GameFoot | null> => {
  try {
    const game = await GameFootModel.findById(id);
    if (game === null) throw Error(`No game found with ID ${id}.`);
    return { ...game.toJSON() };
  } catch (e: unknown) {
    throw Error((e as {message: string}).message);
  }
};

const getByIdCompetition = async (idComp: number): Promise<GameFoot[]> => {
  try {
    // eslint-disable-next-line quote-props
    const games = await GameFootModel.find({ idCompetition: idComp }).lean();
    return games.map((game) => ({ ...game, id: game._id }));
  } catch (e: unknown) {
    throw Error((e as {message: string}).message);
  }
};

const getNextByIdCompetition = async (idComp: number): Promise<GameFoot[]> => {
  try {
    // eslint-disable-next-line quote-props
    const games = await GameFootModel.find({
      $and: [
        { idCompetition: idComp },
        { date: { $gte: new Date() } }],
    }).sort({ date: 'asc' }).lean();
    return games.map((game) => ({ ...game, id: game._id }));
  } catch (e: unknown) {
    throw Error((e as {message: string}).message);
  }
};

const getByIdTeam = async (idTeam: number): Promise<GameFoot[]> => {
  try {
    // eslint-disable-next-line quote-props
    const games = await GameFootModel.find({ $or: [{ 'idTeamHome': idTeam }, { 'idTeamAway': idTeam }] }).lean();
    return games.map((game) => ({ ...game, id: game._id }));
  } catch (e: unknown) {
    throw Error((e as {message: string}).message);
  }
};

const getByDate = async (date: Date): Promise<GameFoot[]> => {
  try {
    // eslint-disable-next-line quote-props
    const gteDate = new Date(date.setHours(0, 0, 0, 0));
    const ltDate = new Date(new Date(date.setDate(date.getDate() + 1)).setHours(0, 0, 0, 0));
    const games = await GameFootModel.find({ date: { $gte: gteDate, $lt: ltDate } }).lean();
    return games.map((game) => ({ ...game, id: game._id }));
  } catch (e: unknown) {
    throw Error((e as {message: string}).message);
  }
};

const getNextByIdTeam = async (idTeam: number): Promise<GameFoot[]> => {
  try {
    // eslint-disable-next-line quote-props
    const games = await GameFootModel.find({
      $and: [
        { $or: [{ 'teams.idTeamHome': idTeam }, { 'teams.idTeamAway': idTeam }] },
        { date: { $gte: new Date() } }],
    }).lean();
    return games.map((game) => ({ ...game, id: game._id }));
  } catch (e: unknown) {
    throw Error((e as {message: string}).message);
  }
};

const getPreviousByIdTeam = async (idTeam: number): Promise<GameFoot[]> => {
  try {
    // eslint-disable-next-line quote-props
    const games = await GameFootModel.find({
      $and: [
        { $or: [{ 'teams.idTeamHome': idTeam }, { 'teams.idTeamAway': idTeam }] },
        { date: { $lte: new Date() } }],
    }).sort({ date: 'desc' }).lean();
    return games.map((game) => ({ ...game, id: game._id }));
  } catch (e: unknown) {
    throw Error((e as {message: string}).message);
  }
};

const getByRoundAndIdComp = async (idComp:number, r: string): Promise<GameFoot[]> => {
  try {
    // eslint-disable-next-line quote-props
    const games = await GameFootModel.find({ $and: [{ idCompetition: idComp }, { round: r }] }).sort({ date: 'asc' }).lean();
    return games.map((game) => ({ ...game, id: game._id }));
  } catch (e: unknown) {
    throw Error((e as {message: string}).message);
  }
};

export default {
  getAll, getById, getByIdCompetition, getNextByIdCompetition, getByIdTeam, getByDate, getPreviousByIdTeam, getNextByIdTeam, getByRoundAndIdComp,
};
