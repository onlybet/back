import controller from './gameFoot.controller';
import service from './gameFoot.service';
import { GameFootSchema, GameFootModel, GameFoot } from './gameFoot.model';

export const foot = {
  controller,
  service,
  schema: GameFootSchema,
  model: GameFootModel,
};

export type { GameFoot };
