import gameFootRepository from './gameFoot.repository';
import { GameFoot } from './gameFoot.model';

const getAll = async (): Promise<GameFoot[]> => {
  try {
    const games = await gameFootRepository.getAll();
    if (games === null) throw Error('No games found');
    return games;
  } catch (e:any) {
    throw Error(e.message);
  }
};

const getGameFootById = async (id: number): Promise<GameFoot> => {
  try {
    const game = await gameFootRepository.getById(id);
    if (game === null) throw Error(`No game found with id ${id}`);
    return game;
  } catch (e:any) {
    throw Error(e.message);
  }
};

const getGameFootByIdCompetition = async (idComp: number): Promise<GameFoot[]> => {
  try {
    const games = await gameFootRepository.getByIdCompetition(idComp);
    if (games === null) throw Error(`No games found with idCompetition ${idComp}`);
    return games;
  } catch (e:any) {
    throw Error(e.message);
  }
};

const getNextGameFootByIdCompetition = async (idComp: number): Promise<GameFoot> => {
  try {
    const games = await gameFootRepository.getByIdCompetition(idComp);
    let nextGame = games[0];
    const today = new Date();
    if (games === null) {
      throw Error(`No games found with idCompetition ${idComp}`);
    } else {
      games.forEach((game) => {
        if (today < game.date && (nextGame.date < today || nextGame.date > game.date)) {
          nextGame = game;
        }
      });
    }
    return nextGame;
  } catch (e:any) {
    throw Error(e.message);
  }
};

const getPreviousGameFootByIdCompetition = async (idComp: number): Promise<GameFoot> => {
  try {
    const games = await gameFootRepository.getByIdCompetition(idComp);
    let nextGame = games[0];
    const today = new Date();
    if (games === null) {
      throw Error(`No games found with idCompetition ${idComp}`);
    } else {
      games.forEach((game) => {
        if (today > game.date && nextGame.date < game.date) {
          nextGame = game;
        }
      });
    }
    return nextGame;
  } catch (e:any) {
    throw Error(e.message);
  }
};

const getGameFootByIdTeam = async (idTeam: number): Promise<GameFoot[]> => {
  try {
    const games = await gameFootRepository.getByIdTeam(idTeam);
    if (games === null) throw Error(`No games found with idTeam ${idTeam}`);
    return games;
  } catch (e:any) {
    throw Error(e.message);
  }
};

const getNextGameFootByIdTeam = async (idTeam: number): Promise<GameFoot[]> => {
  try {
    const games = await gameFootRepository.getNextByIdTeam(idTeam);
    if (games === null) throw Error(`No games found with idTeam ${idTeam}`);
    return games;
  } catch (e:any) {
    throw Error(e.message);
  }
};

const getPreviousGameFootByIdTeam = async (idTeam: number): Promise<GameFoot[]> => {
  try {
    const games = await gameFootRepository.getPreviousByIdTeam(idTeam);
    if (games === null) throw Error(`No games found with idTeam ${idTeam}`);
    return games;
  } catch (e:any) {
    throw Error(e.message);
  }
};

// const getPreviousGameFootByIdTeam = async (idTeam: number): Promise<GameFoot> => {
//   try {
//     const games = await gameFootRepository.getByIdTeam(idTeam);
//     let nextGame = games[0];
//     const today = new Date();
//     if (games === null) {
//       throw Error(`No games found with idTeam ${idTeam}`);
//     } else {
//       games.forEach((game) => {
//         if (today > game.date && nextGame.date < game.date) {
//           nextGame = game;
//         }
//       });
//     }
//     return nextGame;
//   } catch (e:any) {
//     throw Error(e.message);
//   }
// };

const getGameFootByDate = async (date: Date): Promise<GameFoot[]> => {
  try {
    const games = await gameFootRepository.getByDate(date);
    if (games === null) throw Error(`No game found with date ${date}`);
    return games;
  } catch (e:any) {
    throw Error(e.message);
  }
};

const getGameFootByRoundAndIdComp = async (idComp:number, round: string): Promise<GameFoot[]> => {
  try {
    const games = await gameFootRepository.getByRoundAndIdComp(idComp, round);
    return games;
  } catch (e:any) {
    throw Error(e.message);
  }
};

export default {
  getGameFootById,
  getAll,
  getGameFootByIdCompetition,
  getGameFootByIdTeam,
  getNextGameFootByIdCompetition,
  getPreviousGameFootByIdCompetition,
  getNextGameFootByIdTeam,
  getPreviousGameFootByIdTeam,
  getGameFootByDate,
  getGameFootByRoundAndIdComp,
};
