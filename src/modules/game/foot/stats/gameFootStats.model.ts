import { model, Schema } from 'mongoose';
import { GameFoot } from '../gameFoot.model';

export interface GameFootStats extends GameFoot {
  teamHome:{
    shotsOnGoal: number;
    shotsOffGoal: number;
    totalShots: number;
    blockedShots: number;
    shotsInsidebox: number;
    shotsOutsidebox: number;
    fouls: number;
    cornerKicks: number;
    offsides: number;
    ballPossession: string;
    yellowCards: number;
    redCards: number;
    goalkeeperSaves: number;
    totalPasses: number;
    passesAccurate: number;
    passesPourcent: string;
  }
  teamAway:{
    shotsOnGoal: number;
    shotsOffGoal: number;
    totalShots: number;
    blockedShots: number;
    shotsInsidebox: number;
    shotsOutsidebox: number;
    fouls: number;
    cornerKicks: number;
    offsides: number;
    ballPossession: string;
    yellowCards: number;
    redCards: number;
    goalkeeperSaves: number;
    totalPasses: number;
    passesAccurate: number;
    passesPourcent: string;
  }
}

export const GameFootStatsSchema = new Schema<GameFootStats>({
  _id: {
    type: Number, required: true,
  },
  teamHome: {
    shotsOnGoal: { type: Number },
    shotsOffGoal: { type: Number },
    totalShots: { type: Number },
    blockedShots: { type: Number },
    shotsInsidebox: { type: Number },
    shotsOutsidebox: { type: Number },
    fouls: { type: Number },
    cornerKicks: { type: Number },
    offsides: { type: Number },
    ballPossession: { type: String },
    yellowCards: { type: Number },
    redCards: { type: Number },
    goalkeeperSaves: { type: Number },
    totalPasses: { type: Number },
    passesAccurate: { type: Number },
    passesPourcent: { type: String },
  },
  teamAway: {
    shotsOnGoal: { type: Number },
    shotsOffGoal: { type: Number },
    totalShots: { type: Number },
    blockedShots: { type: Number },
    shotsInsidebox: { type: Number },
    shotsOutsidebox: { type: Number },
    fouls: { type: Number },
    cornerKicks: { type: Number },
    offsides: { type: Number },
    ballPossession: { type: String },
    yellowCards: { type: Number },
    redCards: { type: Number },
    goalkeeperSaves: { type: Number },
    totalPasses: { type: Number },
    passesAccurate: { type: Number },
    passesPourcent: { type: String },
  },
}, { collection: 'gamesFootStats', timestamps: true });

export const GameFootStatsModel = model<GameFootStats>('GameFootStats', GameFootStatsSchema);
