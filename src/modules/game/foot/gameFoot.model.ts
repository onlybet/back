import { model, Schema } from 'mongoose';
import { Game } from '../game.model';

export interface GameFoot extends Game {
  teams:{
    idTeamHome: number;
    idTeamAway: number;
  }
  goals:{
    goalsTeamHome: number;
    goalsTeamAway: number;
  }
  longStatus : string;
  shortStatus : string;
  elapsed : number;
}

export const GameFootSchema = new Schema<GameFoot>({
  _id: {
    type: Number, required: true,
  },
  date: {
    type: Date, required: true,
  },
  idCompetition: {
    type: Number, required: true,
  },
  teams: {
    idTeamHome: { type: Number, required: true },
    idTeamAway: { type: Number, required: true },
  },
  goals: {
    goalsTeamHome: {
      type: Number,
    },
    goalsTeamAway: {
      type: Number,
    },
  },
  round: {
    type: String,
  },
  longStatus: {
    type: String, required: true,
  },
  shortStatus: {
    type: String, required: true,
  },
  elapsed: {
    type: Number,
  },
}, { collection: 'gamesFoot', timestamps: true });

export const GameFootModel = model<GameFoot>('GameFoot', GameFootSchema);
