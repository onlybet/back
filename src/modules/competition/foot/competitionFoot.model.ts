import { model, Schema } from 'mongoose';
import { Competition } from '../competition.model';

type APICompetitionType = 'League' | 'Cup';

export interface CompetitionFoot extends Competition {
  type: APICompetitionType;
}

export const CompetitionFootSchema = new Schema<CompetitionFoot>({
  _id: {
    type: Number, required: true,
  },
  name: {
    type: String, required: true, trim: true, set: (n: string) => n.trim().substr(0, 50),
  },
  country: {
    type: String, required: true, trim: true, set: (n: string) => n.trim().substr(0, 50),
  },
  season: {
    type: Number, required: true,
  },
  logo: {
    type: String, required: true,
  },
  type: {
    type: String, required: true, trim: true, set: (n: string) => n.trim().substr(0, 50),
  },
  active: {
    type: Boolean, required: true,
  },
}, { collection: 'competitionsFoot', timestamps: true });

export const CompetitionFootModel = model<CompetitionFoot>('CompetitionFoot', CompetitionFootSchema);
