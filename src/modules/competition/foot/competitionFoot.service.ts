import competitionFootRepository from './competitionFoot.repository';
import { CompetitionFoot } from './competitionFoot.model';

const getAll = async (): Promise<CompetitionFoot[]> => {
  try {
    const competitions = await competitionFootRepository.getAll();
    if (competitions === null) throw Error('No competitions found');
    return competitions;
  } catch (e:any) {
    throw Error(e.message);
  }
};

const getCompetitionFootById = async (id: number): Promise<CompetitionFoot> => {
  try {
    const competition = await competitionFootRepository.getById(id);
    if (competition === null) throw Error(`No competition found with id ${id}`);
    return competition;
  } catch (e:any) {
    throw Error(e.message);
  }
};

const getCompetitionFootByName = async (name: string): Promise<CompetitionFoot[]> => {
  try {
    const competitions = await competitionFootRepository.getByName(name);
    if (competitions === null) throw Error(`No competitions found with name ${name}`);
    return competitions;
  } catch (e:any) {
    throw Error(e.message);
  }
};

export default { getCompetitionFootById, getAll, getCompetitionFootByName };
