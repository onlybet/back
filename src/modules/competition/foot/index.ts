import controller from './competitionFoot.controller';
import service from './competitionFoot.service';
import { CompetitionFootSchema, CompetitionFootModel, CompetitionFoot } from './competitionFoot.model';

export const foot = {
  controller,
  service,
  schema: CompetitionFootSchema,
  model: CompetitionFootModel,
};

export type { CompetitionFoot };
