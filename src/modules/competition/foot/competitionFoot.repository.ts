import { CompetitionFootModel, CompetitionFoot } from './competitionFoot.model';

const getAll = async (): Promise<CompetitionFoot[]> => {
  try {
    const competitions = await CompetitionFootModel.find().lean();
    return competitions.map((competition) => ({ ...competition, id: competition._id }));
  } catch (e: unknown) {
    throw Error((e as {message: string}).message);
  }
};

const getById = async (id: number): Promise<CompetitionFoot | null> => {
  try {
    const competition = await CompetitionFootModel.findById(id);
    if (competition === null) throw Error(`No competition found with ID ${id}.`);
    return { ...competition.toJSON() };
  } catch (e: unknown) {
    throw Error((e as {message: string}).message);
  }
};

const getByName = async (name: string): Promise<CompetitionFoot[]> => {
  try {
    const competitions = await CompetitionFootModel.find({ name: { $regex: name, $options: 'i' } }).lean();
    return competitions.map((competition) => ({ ...competition, id: competition._id }));
  } catch (e: unknown) {
    throw Error((e as {message: string}).message);
  }
};

export default {
  getAll, getById, getByName,
};
