import express, { Request, Response } from 'express';
import competitionFootService from './competitionFoot.service';

const controller = express.Router();

controller.get('/all', async (req: Request, res: Response) => {
  try {
    const result = await competitionFootService.getAll();
    res.status(200).send(result);
  } catch (e) {
    res.status(500).send((e as any).message);
  }
});

controller.get('/:id', async (req: Request, res: Response) => {
  try {
    const result = await competitionFootService.getCompetitionFootById(Number(req.params.id));
    res.status(200).send(result);
  } catch (e) {
    res.status(500).send((e as any).message);
  }
});

controller.get('/name/:name', async (req: Request, res: Response) => {
  try {
    const result = await competitionFootService.getCompetitionFootByName(req.params.name);
    res.status(200).send(result);
  } catch (e) {
    res.status(500).send((e as any).message);
  }
});

export default { instance: controller };
