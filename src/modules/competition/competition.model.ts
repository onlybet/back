export interface Competition{
  _id: number;
  name: string;
  country: string;
  season: number;
  logo: string;
  active: boolean;
}
