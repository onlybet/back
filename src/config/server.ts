import express, { Express } from 'express';
import cors from 'cors';
import helmet from 'helmet';

// configure server
export const initServer = (): Express => {
  console.log(`Listening for address ${process.env.CORS_ADDRESS}`);
  const server = express();
  server.use(helmet());
  server.use(cors({
    origin: process.env.CORS_ADDRESS,
  }));
  server.use(express.json());
  return server;
};
