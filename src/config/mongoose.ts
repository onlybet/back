import mongoose from 'mongoose';

export const connectMongo = (username: string, password:string, cluster:string, database:string) => {
  try {
    const url = `mongodb+srv://${username}:${password}@${cluster}/${database}?retryWrites=true&w=majority`;
    mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
    mongoose.set('useCreateIndex', true);
    mongoose.set('useFindAndModify', false);
    const { connection } = mongoose;

    connection.once('open', () => {
      console.log('Connected to Mongoose Server successfully');
    });
  } catch (err) {
    console.log("Can't connect to database", (err as any).message);
  }
};
