export function isValidDate(date: Date) {
  return date && Object.prototype.toString.call(date) === '[object Date]' && !Number.isNaN(date);
}
