module.exports = {
  env: {
    node: true,
    es2021: true,
  },
  extends: [
    'airbnb-base',
    'plugin:import/typescript',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: [
    '@typescript-eslint', 'import',
  ],
  rules: {
    'no-console': 0,
    '@typescript-eslint/no-use-before-define': ['error'],
    'max-len': ['error', { code: 170, tabWidth: 2 }],
    'import/prefer-default-export': 0,
    'global-require': 0,
    'import/extensions': 0,
    /* eslint-disable linebreak-style */
    'import/no-named-as-default': 0,
    'import/no-named-default': 0,
    'no-underscore-dangle': 0,
    'linebreak-style': ['error', 'unix'],
    'no-shadow': 'off',
    '@typescript-eslint/no-shadow': ['error'],
    'class-methods-use-this': 0,
    'import/order': [
      'error',
      {
        'newlines-between': 'never',
        groups: [
          ['builtin', 'external'],
          ['internal', 'parent', 'sibling', 'index'],
        ],
      },
    ],
  },
  settings: {
    'import/parsers': {
      '@typescript-eslint/parser': ['.ts'],
    },
    'import/resolver': {
      typescript: {
        alwaysTryTypes: true, // always try to resolve types under `<root>@types` directory even it doesn't contain any source code, like `@types/unist`

        // Choose from one of the "project" configs below or omit to use <root>/tsconfig.json by default

        // use <root>/path/to/folder/tsconfig.json
        project: './tsconfig.json',
      },
    },
  },
};
