FROM node:13-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --force

COPY . .

EXPOSE 3000

ENTRYPOINT ["npm"]
CMD [ "run", "dev:linux" ]
